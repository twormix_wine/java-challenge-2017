package just.a.test;

import org.junit.Assert;
import org.junit.Test;

public class ThisTestsSomething {

	@Test
	public void shouldNotSendTransactionMultipleTimes() {
		Assert.assertEquals(2 + 3, 5);
	}

	@Test
	public void testThatFunction2() {
		Assert.assertEquals(2 + 3, 5);
	}

	@Test
	public void testThatFunction3() {
		Assert.assertEquals(2 + 3, 5);
	}

	@Test
	public void testThatFunction4() {
		Assert.assertEquals(2 + 3, 5);
	}

	@Test
	public void testThatFunction5() {
		Assert.assertEquals(2 + 3, 3);
	}

}
