package com.twormix.spacebattle.gui;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class GuiApp extends JFrame {
	private static final long serialVersionUID = 1L;

	public GuiApp(int x, int y, GamePanel panel) {
		setLayout(new BorderLayout());
		setTitle("Twormix Wine");
		setSize(x, y);
		getContentPane().setBackground(Color.BLACK);
		getContentPane().add(panel);
		setVisible(true);
	}

	@Override
	public synchronized void addKeyListener(KeyListener l) {
		super.addKeyListener(l);
	}
}
