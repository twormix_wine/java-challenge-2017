package com.twormix.spacebattle.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;

import com.twormix.spacebattle.FuzzyGameLogic.PlanetAggregateWithScore;
import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;
import com.twormix.spacebattle.messages.PlanetState;

public interface GuiUpdater {
	
	void setUp(GameDescription map);

	void update(GameState update);

	void addKeyListener(KeyListener listener);
}
