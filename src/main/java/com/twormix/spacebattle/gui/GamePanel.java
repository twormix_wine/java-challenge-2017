package com.twormix.spacebattle.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Function;

import javax.swing.JPanel;

import com.twormix.spacebattle.GameLogic;
import com.twormix.spacebattle.Main;
import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;
import com.twormix.spacebattle.messages.Planet;
import com.twormix.spacebattle.messages.PlanetState;
import com.twormix.spacebattle.messages.Player;
import com.twormix.spacebattle.messages.StationedArmy;

public class GamePanel extends JPanel implements GuiUpdater {
	
	private static final long serialVersionUID = 1L;
	private volatile GameState state;
	private volatile int mapWidth = 1;
	private volatile int mapHeight = 1;
	private volatile int gameLength;
	private Map<Integer, Planet> planets = new HashMap<>();
	private GameLogic logic;
	
	
	private final Map<String, Color> colors = new HashMap<>();

	public GamePanel(GameLogic logic) {
		this.logic = logic;
	}
	
	@Override
	public void paint(Graphics g) {

		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());

		if (state == null || planets == null) {
			return;
		}

		final String elapsedTimeStr = " Time remaining: " + (gameLength - state.getTimeElapsed()) / 1000.0;
		final String metadataString = logic.getCurrentStateMetadataJson();

		g.setColor(Color.WHITE);
		g.drawString(state.getStandings().toString() + elapsedTimeStr + " - " + metadataString, 30, 30);

		for (Planet p : planets.values()) {
			drawCircle(g, p.getX(), p.getY(), p.getRadius(), Color.WHITE);
		}

		for (PlanetState p : state.getPlanetStates()) {
			String ptext = "<" + Integer.toString(p.getPlanetID()) + ">  " + p.getOwner();

			Planet planet = planets.get(p.getPlanetID());
			int r = planet.getRadius();
			int rr = (int) (r * p.getOwnershipRatio());

			Color pc = Main.USER_ID.equals(p.getOwner()) ? Color.BLUE : colors.get(p.getOwner());
			drawCircle(g, planet.getX(), planet.getY(), rr, pc);
			drawCenteredString(g, ptext, planet.getX(), planet.getY(), Color.BLACK);

			p.getMovingArmies().forEach(army -> {
				drawArrowFromTo(g, army.getOwner() + "(" + army.getSize() + ")", (int) army.getX(), (int) army.getY(),
						planet.getX(), planet.getY(), Main.USER_ID.equals(army.getOwner()) ? Color.CYAN : colors.get(army.getOwner()));
			});

			String planetMeta = "";
			for (StationedArmy army : p.getStationedArmies()) {
				String armyText = army.getOwner() + " (" + army.getSize() + ")";
				planetMeta += armyText + "\n";
			}
			drawCenteredString(g, planetMeta, planet.getX(), planet.getY() + planet.getRadius(), Color.GRAY);
		}
	}

	@Override
	public void setUp(GameDescription map) {
		this.setDoubleBuffered(true);
		map.getPlanets().stream().forEach(p -> planets.put(p.getPlanetID(), p));
		mapWidth = map.getMapSizeX();
		mapHeight = map.getMapSizeY();
		gameLength = map.getGameLength();
		
		int i = 0;
		Color[] colorss = { Color.MAGENTA, Color.RED, Color.ORANGE, Color.BLUE };
		for (Player player : map.getPlayers()) {
			if (Main.USER_ID.equals(player.getUserID())) {
				continue;
			}
			colors.put(player.getUserID(), colorss[i]);
			i++;
		}
	}

	@Override
	public void update(GameState update) {
		state = update;
		repaint();
	}

	private void drawCircle(Graphics g, int x, int y, int r, Color color) {
		g.setColor(color);

		double xx = (double) (x - r) / mapWidth * this.getWidth();
		double yy = (double) (y - r) / mapHeight * this.getHeight();
		double xr = 2.0 * r / mapWidth * this.getWidth();
		double yr = 2.0 * r / mapHeight * this.getHeight();

		g.fillOval((int) xx, (int) yy, (int) xr, (int) yr);
	}

	private void drawCenteredString(Graphics g, String text, int x, int y, Color color) {

		double rx = (double) x / mapWidth * this.getWidth();
		double ry = (double) y / mapHeight * this.getHeight();

		if (text == null)
			return;
		// Get the FontMetrics
		FontMetrics metrics = g.getFontMetrics(getFont());
		// Determine the X coordinate for the text
		int xx = (int) (rx - metrics.stringWidth(text) / 2);
		int yy = (int) (ry - metrics.getHeight() / 2 + metrics.getAscent());
		// Set the font
		g.setFont(getFont());
		g.setColor(color);
		// Draw the String
		g.drawString(text, xx, yy);
	}

	private void drawArrowFromTo(Graphics g, String text, int sx, int sy, int ex, int ey, Color color) {

		double srx = (double) sx / mapWidth * this.getWidth();
		double sry = (double) sy / mapHeight * this.getHeight();

		double drx = (double) ex / mapWidth * this.getWidth();
		double dry = (double) ey / mapHeight * this.getHeight();

		g.setColor(color);
		g.drawLine((int) srx, (int) sry, (int) drx, (int) dry);
		drawCenteredString(g, text, sx, sy, color);
	}
}