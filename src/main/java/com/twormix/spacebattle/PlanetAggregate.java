package com.twormix.spacebattle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.twormix.spacebattle.messages.MovingArmy;
import com.twormix.spacebattle.messages.Planet;
import com.twormix.spacebattle.messages.PlanetState;

// TODO maxmium ellenséges sereget vegye figyelembe

public class PlanetAggregate {

	private final int x;
	private final int y;
	private final int radius;
	private final PlanetState state;

	public PlanetAggregate(int x, int y, int radius, PlanetState state) {
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.state = state;
	}

	public PlanetAggregate(PlanetAggregate other) {
		this.x = other.x;
		this.y = other.y;
		this.radius = other.radius;
		this.state = other.state;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getRadius() {
		return radius;
	}

	public PlanetState getState() {
		return state;
	}

	public boolean fullyOwnedBy(String teamId) {
		return teamId.equals(state.getOwner()) && state.getOwnershipRatio() == 1.0;
	}

	public boolean fullyOwnedByOthers(String teamId) {
		return !teamId.equals(state.getOwner()) && state.getOwnershipRatio() == 1.0;
	}

	public int armiesAgainst(String teamId) {
		return state.getStationedArmies().stream() //
				.filter(army -> !teamId.equals(army.getOwner())) //
				.map(army -> army.getSize()) //
				.reduce(0, (aa, bb) -> aa + bb);
	}

	public int armyFor(String teamId) {
		return state.getStationedArmies().stream() //
				.filter(army -> teamId.equals(army.getOwner())) //
				.map(army -> army.getSize()) //
				.findFirst().orElse(0);
	}

	public int incomingFor(String teamId) {
		return state.getMovingArmies().stream() //
				.filter(army -> teamId.equals(army.getOwner())) //
				.map(army -> army.getSize()) //
				.reduce(0, (a, b) -> a + b);
	}

	/**
	 * Gives back the army incoming in the given time window
	 * 
	 * @param teamId
	 * @param ms
	 *            the time window
	 * @return
	 */
	public int incomingFor(String teamId, double speed, int ms) {
		double sec = ms / 1000;
		return state.getMovingArmies().stream() //
				.filter(army -> teamId.equals(army.getOwner())) //
				.filter(army -> distanceTo(army) / speed < sec) //
				.map(army -> army.getSize()) //
				.reduce(0, (a, b) -> a + b);
	}

	public int incomingAgainst(String teamId) {

		Map<String, List<Integer>> incomingArmies = new HashMap<>();

		for (MovingArmy army : state.getMovingArmies()) {

			if (teamId.equals(army.getOwner())) {
				continue;
			}

			List<Integer> l = incomingArmies.get(army.getOwner());
			if (l == null) {
				l = new ArrayList<>();
			}

			l.add(army.getSize());

			incomingArmies.put(army.getOwner(), l);
		}

		Map<String, Integer> sumArmies = new HashMap<>();

		for (String army : incomingArmies.keySet()) {
			List<Integer> sizes = incomingArmies.get(army);
			int sum = 0;

			for (int size : sizes) {
				sum += size;
			}
			sumArmies.put(army, sum);
		}

		return sumArmies.values().stream().max((a, b) -> (a > b ? -1 : 1)).orElse(0);

		// return state.getMovingArmies().stream() //
		// .filter(army -> !teamId.equals(army.getOwner())) //
		// .map(army -> army.getSize()) //
		// .reduce(0, (aa, bb) -> aa + bb);
	}

	public int incomingAgainst(String teamId, double speed, int ms) {
		double sec = ms / 1000;

		Map<String, List<Integer>> incomingArmies = new HashMap<>();

		for (MovingArmy army : state.getMovingArmies()) {

			if (teamId.equals(army.getOwner())) {
				continue;
			}

			if (distanceTo(army) / speed > sec) {
				continue;
			}

			List<Integer> l = incomingArmies.get(army.getOwner());
			if (l == null) {
				l = new ArrayList<>();
			}

			l.add(army.getSize());

			incomingArmies.put(army.getOwner(), l);
		}

		Map<String, Integer> sumArmies = new HashMap<>();

		for (String army : incomingArmies.keySet()) {
			List<Integer> sizes = incomingArmies.get(army);
			int sum = 0;

			for (int size : sizes) {
				sum += size;
			}
			sumArmies.put(army, sum);
		}

		return sumArmies.values().stream().max((a, b) -> (a > b ? -1 : 1)).orElse(0);

		// return state.getMovingArmies().stream() //
		// .filter(army -> !teamId.equals(army.getOwner())) //
		// .filter(army -> distanceTo(army) / speed < sec) //
		// .map(army -> army.getSize()) //
		// .reduce(0, (aa, bb) -> aa + bb);
	}

	private double distanceTo(MovingArmy army) {
		double dx = army.getX() - x;
		double dy = army.getY() - y;
		return Math.sqrt(dx * dx + dy * dy);
	}

	public static double distance(PlanetAggregate a, PlanetAggregate b) {
		final int xd = a.getX() - b.getX();
		final int yd = a.getY() - b.getY();
		return Math.sqrt(xd * xd + yd * yd);
	}

	public static double distance(Planet a, Planet b) {
		final int xd = a.getX() - b.getX();
		final int yd = a.getY() - b.getY();
		return Math.sqrt(xd * xd + yd * yd);
	}

	@Override
	public String toString() {
		return "{ \"x\":" + x + ", \"y\":" + y + ", \"radius\":" + radius + ", \"state\":" + state + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanetAggregate other = (PlanetAggregate) obj;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (state.getPlanetID() != other.state.getPlanetID())
			return false;
		return true;
	}
}
