package com.twormix.spacebattle;

import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;

public class FixedGameLogic implements GameLogic {

	public FixedGameLogic(CommandSender command, String userId) {
		
	}

	@Override
	public void update(GameState state) {
		
	}

	@Override
	public void setUp(GameDescription description) {
		
	}

	@Override
	public String getCurrentStateMetadataJson() {
		return null;
	}

	@Override
	public String getCurrentParametersJson() {
		return "{}";
	}

}
