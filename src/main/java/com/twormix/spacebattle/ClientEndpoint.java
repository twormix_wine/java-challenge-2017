package com.twormix.spacebattle;

import java.io.IOException;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twormix.spacebattle.gui.GuiApp;
import com.twormix.spacebattle.gui.GuiUpdater;
import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;

public class ClientEndpoint extends Endpoint implements MessageHandler.Whole<String> {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Session session;

	private final ObjectMapper mapper = new ObjectMapper();

	private CommandSender command;
	private GameRunner runner;

	public ClientEndpoint(GameRunner runner, CommandSender command) {
		this.runner = runner;
		this.command = command;
	}

	@Override
	public void onOpen(Session session, EndpointConfig config) {
		this.session = session;
		this.session.addMessageHandler(this);
		command.setSession(this.session);
	}

	@Override
	public void onMessage(String message) {
		logger.debug("Message received: {}", message);
		try {
			if (message.contains("commandSchedule")) { // somehow differentiate json 'types'
				runner.setUp(mapper.readValue(message, GameDescription.class));
			} else {
				runner.updateCallback(mapper.readValue(message, GameState.class));
			}
		} catch (IOException e) {
			logger.error("Wtf... {}", e.getMessage());
		}
	}
}
