package com.twormix.spacebattle;

import java.util.concurrent.ExecutionException;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twormix.spacebattle.messages.MoveCommand;

public class CommandSender {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Session session;
	private ObjectMapper mapper = new ObjectMapper();

	public void setSession(Session session) {
		this.session = session;
	}

	public void sendCommand(MoveCommand command) {
		logger.info("Sending command {}", command);
		try {
			session.getAsyncRemote().sendText(mapper.writeValueAsString(command)).get();
		} catch (JsonProcessingException | InterruptedException | ExecutionException e) {
			logger.error("Shit happens... {}", e.getMessage());
		}
	}
}
