package com.twormix.spacebattle.messages;

public enum GameStatus {
	PLAYING, ENDED, ABORTED;
}
