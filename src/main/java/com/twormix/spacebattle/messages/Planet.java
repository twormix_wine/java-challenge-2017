package com.twormix.spacebattle.messages;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Planet {

	private @JsonProperty int planetID;
	private @JsonProperty int x;
	private @JsonProperty int y;
	private @JsonProperty int radius;

	public int getPlanetID() {
		return planetID;
	}

	public void setPlanetID(int planetID) {
		this.planetID = planetID;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}

}
