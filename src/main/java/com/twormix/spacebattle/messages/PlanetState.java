package com.twormix.spacebattle.messages;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PlanetState {
	private @JsonProperty int planetID;
	private @JsonProperty String owner;
	private @JsonProperty double ownershipRatio;
	private @JsonProperty List<MovingArmy> movingArmies; // armies moving towards this planet
	private @JsonProperty List<StationedArmy> stationedArmies; // armies on this planet

	public int getPlanetID() {
		return planetID;
	}

	public void setPlanetID(int planetID) {
		this.planetID = planetID;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getOwnershipRatio() {
		return ownershipRatio;
	}

	public void setOwnershipRatio(double ownershipRatio) {
		this.ownershipRatio = ownershipRatio;
	}

	public List<MovingArmy> getMovingArmies() {
		return movingArmies;
	}

	public void setMovingArmies(List<MovingArmy> movingArmies) {
		this.movingArmies = movingArmies;
	}

	public List<StationedArmy> getStationedArmies() {
		return stationedArmies;
	}

	public void setStationedArmies(List<StationedArmy> stationedArmies) {
		this.stationedArmies = stationedArmies;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}

}
