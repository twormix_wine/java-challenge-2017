package com.twormix.spacebattle.messages;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GameState {

	private @JsonProperty List<PlanetState> planetStates;
	private @JsonProperty List<Standing> standings;
	private @JsonProperty GameStatus gameStatus;
	private @JsonProperty int timeElapsed;
	private @JsonProperty int remainingPlayers;

	public List<PlanetState> getPlanetStates() {
		return planetStates;
	}

	public void setPlanetStates(List<PlanetState> planetStates) {
		this.planetStates = planetStates;
	}

	public List<Standing> getStandings() {
		return standings;
	}

	public void setStandings(List<Standing> standings) {
		this.standings = standings;
	}

	public GameStatus getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(GameStatus gameStatus) {
		this.gameStatus = gameStatus;
	}

	public int getTimeElapsed() {
		return timeElapsed;
	}

	public void setTimeElapsed(int timeElapsed) {
		this.timeElapsed = timeElapsed;
	}

	public int getRemainingPlayers() {
		return remainingPlayers;
	}

	public void setRemainingPlayers(int remainingPlayers) {
		this.remainingPlayers = remainingPlayers;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
}
