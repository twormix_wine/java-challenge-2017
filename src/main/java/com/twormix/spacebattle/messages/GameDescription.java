package com.twormix.spacebattle.messages;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GameDescription {

	private @JsonProperty int gameLength;
	private @JsonProperty int mapSizeX;
	private @JsonProperty int mapSizeY;
	private @JsonProperty int commandSchedule; // time schedule to process commands
	private @JsonProperty int internalSchedule; // time to update internal state (server)
	private @JsonProperty int broadcastSchedule; // time to broadcast messages from server to client
	private @JsonProperty int minMovableArmySize;
	private @JsonProperty double movementSpeed;
	private @JsonProperty double battleSpeed;
	private @JsonProperty double captureSpeed;
	private @JsonProperty double unitCreateSpeed;
	private @JsonProperty double planetExponent;
	private @JsonProperty double battleExponent;
	private @JsonProperty List<Planet> planets;
	private @JsonProperty List<Player> players;

	public int getGameLength() {
		return gameLength;
	}

	public void setGameLength(int gameLength) {
		this.gameLength = gameLength;
	}

	public int getMapSizeX() {
		return mapSizeX;
	}

	public void setMapSizeX(int mapSizeX) {
		this.mapSizeX = mapSizeX;
	}

	public int getMapSizeY() {
		return mapSizeY;
	}

	public void setMapSizeY(int mapSizeY) {
		this.mapSizeY = mapSizeY;
	}

	public int getCommandSchedule() {
		return commandSchedule;
	}

	public void setCommandSchedule(int commandSchedule) {
		this.commandSchedule = commandSchedule;
	}

	public int getInternalSchedule() {
		return internalSchedule;
	}

	public void setInternalSchedule(int internalSchedule) {
		this.internalSchedule = internalSchedule;
	}

	public int getBroadcastSchedule() {
		return broadcastSchedule;
	}

	public void setBroadcastSchedule(int broadcastSchedule) {
		this.broadcastSchedule = broadcastSchedule;
	}

	public int getMinMovableArmySize() {
		return minMovableArmySize;
	}

	public void setMinMovableArmySize(int minMovableArmySize) {
		this.minMovableArmySize = minMovableArmySize;
	}

	public double getMovementSpeed() {
		return movementSpeed;
	}

	public void setMovementSpeed(double movementSpeed) {
		this.movementSpeed = movementSpeed;
	}

	public double getBattleSpeed() {
		return battleSpeed;
	}

	public void setBattleSpeed(double battleSpeed) {
		this.battleSpeed = battleSpeed;
	}

	public double getCaptureSpeed() {
		return captureSpeed;
	}

	public void setCaptureSpeed(double captureSpeed) {
		this.captureSpeed = captureSpeed;
	}

	public double getUnitCreateSpeed() {
		return unitCreateSpeed;
	}

	public void setUnitCreateSpeed(double unitCreateSpeed) {
		this.unitCreateSpeed = unitCreateSpeed;
	}

	public double getPlanetExponent() {
		return planetExponent;
	}

	public void setPlanetExponent(double planetExponent) {
		this.planetExponent = planetExponent;
	}

	public double getBattleExponent() {
		return battleExponent;
	}

	public void setBattleExponent(double battleExponent) {
		this.battleExponent = battleExponent;
	}

	public List<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(List<Planet> planets) {
		this.planets = planets;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	
	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
}
