package com.twormix.spacebattle.messages;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MoveCommand {
	private final int moveFrom;
	private final int moveTo;
	private final int armySize;

	public MoveCommand(int moveFrom, int moveTo, int armySize) {
		super();
		this.moveFrom = moveFrom;
		this.moveTo = moveTo;
		this.armySize = armySize;
	}

	@JsonGetter
	public int getMoveFrom() {
		return moveFrom;
	}

	@JsonGetter
	public int getMoveTo() {
		return moveTo;
	}

	@JsonGetter
	public int getArmySize() {
		return armySize;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
}
