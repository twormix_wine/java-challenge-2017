package com.twormix.spacebattle;

import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.twormix.spacebattle.gui.GuiUpdater;
import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;
import com.twormix.spacebattle.messages.GameStatus;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.IOException;

public class GameRunner {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final GuiUpdater gui;
	private final GameLogic logic;

	private volatile boolean updated = false;
	private volatile GameState state;
	private volatile boolean setup = false;

	private volatile Path logfile;

	public GameRunner(GameLogic logic, GuiUpdater gui) {
		this.logic = logic;
		this.gui = gui;
	}

	public void writeFileLog(String log) {
		if (logfile == null)
			return;
		try {
			Files.write(logfile, Arrays.asList(log.toString()), UTF_8, APPEND, CREATE);
		} catch (IOException e) {
			logger.debug("Could not write log");
		}
	}

	public synchronized void setUp(GameDescription description) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.hh-mm-ss");
		String formattedString = LocalDateTime.now().format(formatter);
		this.logfile = Paths.get("game-" + formattedString + ".log");
		writeFileLog(description.toString());

		gui.setUp(description);
		logic.setUp(description);
		setup = true;
		logger.info("Game set up with {}", description);
	}

	public synchronized void updateCallback(GameState state) {
		this.state = state;
		writeFileLog(state.toString());
		gui.update(state);
		updated = true;
	}

	public void updateLogic() {

		if (!updated) {
			return;
		}

		if (!setup || state == null) {
			return;
		}

		if (!GameStatus.PLAYING.equals(state.getGameStatus())) {
			return;
		}

		logic.update(state);
		updated = false;
	}
	
	public void saveLogs() {
		int ourScore = state.getStandings().stream().filter(s -> Main.USER_ID.equals(s.getUserID()))
				.map(s -> s.getScore()).findFirst().orElse(0);

		Path gameScores = Paths.get("gamescores.log");
		try {
			Files.write(gameScores, Arrays.asList(ourScore + " --- " + logfile.toString() + " --- " + logic.getCurrentParametersJson()), UTF_8, APPEND, CREATE);
		} catch (IOException e) {
			logger.error("Cannot save scores logfile", e);
		}

		Path renamedLogFile = Paths.get(ourScore + "_SCORE_" + logfile.toString());
		try {
			Files.move(logfile, renamedLogFile, StandardCopyOption.ATOMIC_MOVE);
		} catch (IOException e) {
			logger.error("Cannot save/rename logfile", e);
		}
	}

	public boolean isGameEndedOrAborted() {
		return setup && state != null
				&& (GameStatus.ENDED.equals(state.getGameStatus()) || GameStatus.ABORTED.equals(state.getGameStatus()));
	}
}
