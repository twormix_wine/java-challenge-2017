package com.twormix.spacebattle;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.temporal.TemporalAmount;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twormix.spacebattle.gui.GamePanel;
import com.twormix.spacebattle.gui.GuiApp;
import com.twormix.spacebattle.gui.GuiUpdater;
import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;

public class Main {

	public static final String USER_ID = "twormix1";
	public static final String PASSWORD = "bHeSIENKush6XO3TH";
	public static final String WS_URL = "ws://javachallenge.loxon.hu:8080/JavaChallenge2017/websocket";

	public static class ReplayKeyListener implements KeyListener {

		public volatile double replayDelta = 1;

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyChar() == 's') {
				replayDelta = 0;
			}

			if (e.getKeyChar() == 'd') {
				replayDelta = replayDelta <= 0 ? 1 : replayDelta * 0.5;
			}

			if (e.getKeyChar() == 'a') {
				replayDelta = replayDelta >= 0 ? -1 : replayDelta * 0.5;
			}
		}
	}

	public static void main(String[] args) throws IOException, DeploymentException, InterruptedException {

		final CommandSender command = new CommandSender();
		final GameLogic logic = new FuzzyGameLogic(command, USER_ID);
		final GamePanel gui = new GamePanel(logic);
		final GuiApp app = new GuiApp(512, 512, gui);
		final GameRunner runner = new GameRunner(logic, gui);

		if (args.length == 2 && "replay".equalsIgnoreCase(args[0])) {
			final String logFile = args[1];
			runReplay(logFile, gui, app);
		}

		final String authHeader = USER_ID + ":" + PASSWORD;

		WebSocketContainer webSocket = ContainerProvider.getWebSocketContainer();
		ClientEndpointConfig.Configurator configurator = new ClientEndpointConfig.Configurator() {
			@Override
			public void beforeRequest(Map<String, List<String>> headers) {
				headers.put("Authorization",
						Arrays.asList("Basic " + DatatypeConverter.printBase64Binary(authHeader.getBytes())));
			}
		};
		ClientEndpointConfig config = ClientEndpointConfig.Builder.create().configurator(configurator).build();

		final ClientEndpoint client = new ClientEndpoint(runner, command);
		webSocket.connectToServer(client, config, URI.create(WS_URL));

		while (!runner.isGameEndedOrAborted()) {
			try {
				runner.updateLogic();
			} catch (Exception e) {

			}
		}
		runner.saveLogs();
	}

	private static void runReplay(String logFile, final GamePanel gui, final GuiApp app)
			throws IOException, JsonParseException, JsonMappingException, InterruptedException {
		final List<String> data = Files.lines(Paths.get(logFile)).collect(Collectors.toList());
		ObjectMapper mapper = new ObjectMapper();

		GameDescription desc = mapper.readValue(data.get(0), GameDescription.class);

		ReplayKeyListener keyListener = new ReplayKeyListener();
		gui.setUp(desc);
		app.addKeyListener(keyListener);

		List<GameState> states = data.subList(1, data.size()).stream().map(s -> {
			try {
				return mapper.readValue(s, GameState.class);
			} catch (IOException e) {
				return null;
			}
		}).filter(s -> s != null).collect(Collectors.toList());

		int framePosition = 0;

		while (true) {
			if (keyListener.replayDelta > 0) {
				framePosition++;
			} else if (keyListener.replayDelta < 0) {
				framePosition--;
			}
			framePosition = Math.min(framePosition, states.size() - 1);
			framePosition = Math.max(framePosition, 0);

			gui.update(states.get(framePosition));

			Thread.sleep((long) Math.abs(desc.getBroadcastSchedule() * keyListener.replayDelta));
		}
	}
}
