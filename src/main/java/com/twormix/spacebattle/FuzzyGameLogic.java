package com.twormix.spacebattle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;
import com.twormix.spacebattle.messages.MoveCommand;
import com.twormix.spacebattle.messages.Planet;
import com.twormix.spacebattle.messages.PlanetState;

public class FuzzyGameLogic implements GameLogic {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ObjectMapper jsonMapper = new ObjectMapper();

	public static enum Strategy {
		CAPTURE, CONTROL;
	}

	private volatile Strategy strategy = Strategy.CAPTURE;

	private CommandSender command;
	private GameDescription description;
	private GameState lastState;

	private final Function<PlanetState, PlanetAggregate> toAggregate = planet -> {
		for (Planet p : description.getPlanets()) {
			if (planet.getPlanetID() == p.getPlanetID()) {
				return new PlanetAggregate(p.getX(), p.getY(), p.getRadius(), planet);
			}
		}
		return null;
	};

	private String teamId;
	private int maxRadius;
	private int minRadius;
	private double maxDistance;
	private double minDistance;

	private volatile int fullArmy;
	private volatile int fullEnemy;

	private final double SMOOTH_FACTOR = 50.0;

	private final double TRYHARD_FACTOR = 3; // incoming allies
	private final double RADIUS_FACTOR = 0.2;
	private final double PASSIVE_FACTOR = 1.0;
	private final double ARMY_STRENGTH_SCALE = 50;
	private final double DEFENSE_FACTOR = 2;
	private final double MAX_ARMY_ON_PLANET_FACTOR = 49;
	private final double AGGRESSIVE_FACTOR = 5;
	private final double ENEMY_LOSING_SCALE = 3.5;
	private final double ENEMY_DOMINATING_SCALE = 2;
	private final int IDLE_TIME = 1000;
	private final int DEFENSIVE_IDLE_TIME = 1000;
	private final double MIN_SCORE_TO_SELECT_TARGET = 1;
	private final int CONTROL_ARMY = 5;
	private final double WATCHDOG_STEP = 5; // first target is a planet with less incoming army
	
//	private final double SMOOTH_FACTOR = 50.0;
//
//	private final double TRYHARD_FACTOR = 3; // incoming allies
//	private final double RADIUS_FACTOR = 0.2;
//	private final double PASSIVE_FACTOR = 1.0;
//	private final double ARMY_STRENGTH_SCALE = 50;
//	private final double DEFENSE_FACTOR = 8;
//	private final double MAX_ARMY_ON_PLANET_FACTOR = 49;
//	private final double AGGRESSIVE_FACTOR = 5;
//	private final double ENEMY_LOSING_SCALE = 3.5;
//	private final double ENEMY_DOMINATING_SCALE = 2;
//	private final int IDLE_TIME = 1000;
//	private final int DEFENSIVE_IDLE_TIME = 1000;
//	private final double MIN_SCORE_TO_SELECT_TARGET = 1;
//	private final int CONTROL_ARMY = 5;
//	private final double WATCHDOG_STEP = 5; // first target is a planet with less incoming army


	private final Set<PlanetAggregate> doNotSendFromPlanets = new HashSet<>();

	public FuzzyGameLogic(CommandSender command, String teamId) {
		this.command = command;
		this.teamId = teamId;
	}

	public static class PlanetAggregateWithScore extends PlanetAggregate {

		private double score = 0;
		private int minArmyToSend = 0;

		public PlanetAggregateWithScore(PlanetAggregateWithScore other) {
			super(other);
			this.score = other.score;
		}

		public PlanetAggregateWithScore(int x, int y, int radius, PlanetState state) {
			super(x, y, radius, state);
		}

		public double getScore() {
			return score;
		}

		public void setScore(double score) {
			this.score = score;
		}

		public int getMinArmyToSend() {
			return minArmyToSend;
		}

		public void setMinArmyToSend(int minArmyToSend) {
			this.minArmyToSend = minArmyToSend;
		}
	}

	@Override
	public void update(GameState state) {

		lastState = state;

		logger.info("New round");

		final List<PlanetAggregate> planetsWithOwnArmies = state.getPlanetStates().stream() //
				.map(toAggregate) //
				.filter(planet -> planet.getState().getStationedArmies().stream()
						.anyMatch(army -> teamId.equals(army.getOwner()) && army.getSize() > 0)) //
				.filter(p -> p != null) //
				.collect(Collectors.toList());

		fullArmy = state.getPlanetStates().stream() //
				.map(toAggregate) //
				.map(p -> p.armyFor(teamId) + p.incomingFor(teamId)) //
				.reduce(0, (a, b) -> a + b);

		fullEnemy = state.getPlanetStates().stream() //
				.map(toAggregate) //
				.map(p -> p.armiesAgainst(teamId) + p.incomingAgainst(teamId)) //
				.reduce(0, (a, b) -> a + b);

		int ownScore = state.getStandings().stream().filter(s -> teamId.equals(s.getUserID())).map(s -> s.getStrength())
				.findFirst().orElse(0);
		int enemyScore = state.getStandings().stream().filter(s -> !teamId.equals(s.getUserID()))
				.map(s -> s.getStrength()).sorted((a, b) -> a > b ? -1 : 1).findFirst().orElse(0);

		boolean controlEntryCondition = ownScore > enemyScore * ENEMY_LOSING_SCALE;
		boolean controlExitCondition = ownScore < enemyScore * ENEMY_DOMINATING_SCALE;

		if (controlEntryCondition && Strategy.CAPTURE.equals(strategy)) {
			strategy = Strategy.CONTROL;
		}

		if (controlExitCondition && Strategy.CONTROL.equals(strategy)) {
			strategy = Strategy.CAPTURE;
		}

		if (Strategy.CONTROL.equals(strategy)) {
			controlStrategy(planetsWithOwnArmies, state);
			captureStrategy(planetsWithOwnArmies, state);
		} else {
			captureStrategy(planetsWithOwnArmies, state);
		}
	}

	private void controlStrategy(List<PlanetAggregate> planetsWithOwnArmies, GameState state) {

		final List<PlanetAggregateWithScore> globalScores = state.getPlanetStates().stream() //
				.map(toAggregate) //
				.map(aggr -> new PlanetAggregateWithScore(aggr.getX(), aggr.getY(), aggr.getRadius(), aggr.getState())) //
				.collect(Collectors.toList());

		List<PlanetAggregateWithScore> enemiesOnPlanets = globalScores.stream().map(p -> {
			p.setScore(p.armiesAgainst(teamId) + p.incomingAgainst(teamId));
			return p;
		}).collect(Collectors.toList());

		enemiesOnPlanets.sort((a, b) -> a.getScore() > b.getScore() ? -1 : 1);

		final Map<PlanetAggregate, Integer> sentFromPlanets = new HashMap<>();

		if (doNotSendFromPlanets.size() == state.getPlanetStates().size()) {
			doNotSendFromPlanets.clear();
		}

		doNotSendFromPlanets.addAll(state.getPlanetStates().stream() //
				.map(toAggregate).filter(aggr -> !planetsWithOwnArmies.contains(aggr)).collect(Collectors.toList()));

		for (PlanetAggregateWithScore enemyPlanet : enemiesOnPlanets) {
			int enemySize = (int) enemyPlanet.getScore();
			if (enemySize <= /* enemyPlanet.armyFor(teamId) */ +enemyPlanet.incomingFor(teamId)) {
				continue; // do not send more if enough is sent
			}

			int armySent = enemyPlanet.incomingFor(teamId);
			List<PlanetAggregate> sortedByDistance = new ArrayList<>(planetsWithOwnArmies);
			sortedByDistance.sort((a,
					b) -> PlanetAggregate.distance(enemyPlanet, a) < PlanetAggregate.distance(enemyPlanet, b) ? -1 : 1);
			for (PlanetAggregate ownPlanet : sortedByDistance.subList(0, 1)) {

				if (doNotSendFromPlanets.contains(ownPlanet)) {
					continue;
				}

				int ownArmy = ownPlanet.armyFor(teamId) - sentFromPlanets.getOrDefault(ownPlanet, 0);
				int enemyArmy = ownPlanet.armiesAgainst(teamId);
				int incomingEnemyArmy = ownPlanet.incomingAgainst(teamId);
				int incomingAlly = ownPlanet.incomingFor(teamId, description.getMovementSpeed(), IDLE_TIME);

				int maxArmyToSend = Math.max(0, ownArmy);

				if (ownArmy + incomingAlly > enemyArmy && !ownPlanet.fullyOwnedBy(teamId)) {
					continue; // defend while battle
				}

				if (ownArmy > incomingEnemyArmy) {
					maxArmyToSend = ownArmy - incomingEnemyArmy;
				}

				int toSend = Math.min(enemySize + CONTROL_ARMY, maxArmyToSend);

				if (toSend > description.getMinMovableArmySize()) {
					command.sendCommand(new MoveCommand(ownPlanet.getState().getPlanetID(),
							enemyPlanet.getState().getPlanetID(), toSend));

					doNotSendFromPlanets.add(ownPlanet);

					sentFromPlanets.put(ownPlanet, sentFromPlanets.getOrDefault(ownPlanet, 0) + toSend);

					armySent += toSend;
				}
				if (armySent >= enemySize) {
					break;
				}
			}
		}
	}

	private void captureStrategy(List<PlanetAggregate> planetsWithOwnArmies, GameState state) {
		final List<PlanetAggregateWithScore> globalScores = state.getPlanetStates().stream() //
				.map(toAggregate) //
				.map(aggr -> new PlanetAggregateWithScore(aggr.getX(), aggr.getY(), aggr.getRadius(), aggr.getState())) //
				.collect(Collectors.toList());

		globalScores(globalScores);
		// smoothScores(globalScores);

		for (PlanetAggregate planet : planetsWithOwnArmies) {

			int toStay = armyToStay(planet);

			List<PlanetAggregateWithScore> scoresForOtherPlanets = localScores(planet, globalScores);

			scoresForOtherPlanets = scoresForOtherPlanets.stream()
					.filter(ps -> ps.getScore() > MIN_SCORE_TO_SELECT_TARGET).collect(Collectors.toList());

			if (scoresForOtherPlanets.isEmpty()) {
				continue;
			}

			if (toStay < planet.armyFor(teamId)) {

				final int armyToSplit = planet.armyFor(teamId) - toStay;
				if (armyToSplit < description.getMinMovableArmySize()) {
					continue;
				}

				// calculate scores for targets
				double sumScores = 0;
				final int numArmies = Math.min(scoresForOtherPlanets.size(), splitArmy(planet, scoresForOtherPlanets)); // TODO
				PlanetAggregateWithScore[] target = new PlanetAggregateWithScore[numArmies];
				for (int i = 0; i < numArmies; i++) {
					target[i] = scoresForOtherPlanets.get(i);
					sumScores += target[i].getScore();
				}

				int sentFrom = 0;

				for (int i = 0; i < numArmies; i++) {
					int armySize = Math.max(target[i].getMinArmyToSend(),
							(int) (target[i].getScore() / sumScores * armyToSplit));
					armySize = Math.max(armySize, description.getMinMovableArmySize());
					if (armySize < target[i].armiesAgainst(teamId) + target[i].incomingAgainst(teamId)) {
						continue;
					}
					logger.info("Army to send from {} to {}: {} - full army on planet: {}, army to split: {}",
							planet.getState().getPlanetID(), target[i].getState().getPlanetID(), armySize,
							planet.armyFor(teamId), armyToSplit);

					command.sendCommand(new MoveCommand( //
							planet.getState().getPlanetID(), //
							target[i].getState().getPlanetID(), //
							armySize));

					sentFrom += armySize;
				}

				if (sentFrom > 0) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return;
				}
			}
		}
	}

	private int splitArmy(PlanetAggregate planet, List<PlanetAggregateWithScore> scoresForOtherPlanets) {
		
		long sadLife = scoresForOtherPlanets.stream().filter(
				p -> teamId.equals(p.getState().getOwner()) && 
				p.getState().getOwnershipRatio() > 0.7 && 
				p.getState().getOwnershipRatio() < 1 &&
				p.armiesAgainst(teamId) == 0).count();
				
		int army = planet.armyFor(teamId);
		return (int) Math.max(sadLife, army > MAX_ARMY_ON_PLANET_FACTOR ? 2 : army > MAX_ARMY_ON_PLANET_FACTOR / 2 ? 1 : 1);
	}

	private int armyToStay(PlanetAggregate planet) {
		boolean fullyOwned = (teamId.equals(planet.getState().getOwner())
				&& planet.getState().getOwnershipRatio() == 1.0);

		boolean canGo = planet.armyFor(teamId) >= description.getMinMovableArmySize();

		if (!canGo) {
			return planet.armyFor(teamId);
		}

		boolean losingBattle = planet.armiesAgainst(teamId) + planet.incomingAgainst(teamId,
				description.getMovementSpeed(), DEFENSIVE_IDLE_TIME) > planet.armyFor(teamId)
						+ planet.incomingFor(teamId, description.getMovementSpeed(), DEFENSIVE_IDLE_TIME);

		if (losingBattle) {
			return 0;
		}

		boolean lessEnemyIncoming = planet.incomingAgainst(teamId) <= planet.armyFor(teamId)
				&& planet.incomingAgainst(teamId) > 0;

		if (fullyOwned && lessEnemyIncoming) {
			return planet.incomingAgainst(teamId);
		}

		if (fullyOwned && planet.incomingAgainst(teamId) == 0) {
			return 0;
		}

		return Math.min(planet.armyFor(teamId), (int) (MAX_ARMY_ON_PLANET_FACTOR));
	}

	public void setScoreFor(PlanetAggregateWithScore planetWithScore) {
		double score = 0;

		score += RADIUS_FACTOR * ((double) planetWithScore.getRadius() - minRadius) / ((double) maxRadius - minRadius);
		score += planetWithScore.getState().getOwner() == null ? PASSIVE_FACTOR : 0.0;
		score -= planetWithScore.fullyOwnedBy(teamId) && planetWithScore.incomingAgainst(teamId) == 0 ? 5 : 0;
		planetWithScore.setScore(score);
	}

	private void globalScores(final List<PlanetAggregateWithScore> globalScores) {
		for (PlanetAggregateWithScore planetWithScore : globalScores) {
			setScoreFor(planetWithScore);
		}
	}

	private List<PlanetAggregateWithScore> localScores(PlanetAggregate currentPlanet,
			List<PlanetAggregateWithScore> globalScores) {

		List<PlanetAggregateWithScore> planetsWithScores = globalScores.stream() //
				.filter(p -> p.getState().getPlanetID() != currentPlanet.getState().getPlanetID()) //
				.map(p -> new PlanetAggregateWithScore(p)) // create a copy
				.collect(Collectors.toList());

		for (PlanetAggregateWithScore target : planetsWithScores) {

			double dist = PlanetAggregate.distance(target, currentPlanet);

			boolean isOurArmyBigger = currentPlanet.armyFor(teamId) > target.armiesAgainst(teamId);

			boolean isOwned = teamId.equals(target.getState().getOwner());
			double score = target.getScore();

			double distF = currentPlanet.getState().getPlanetID() == target.getState().getPlanetID() ? 1
					: (dist - maxDistance) / (minDistance - maxDistance); // [0..1]
			distF = distF * distF;

			score += !isOurArmyBigger ? 0
					: AGGRESSIVE_FACTOR * distF * (isOwned ? 1 - target.getState().getOwnershipRatio() : 1);

			score += TRYHARD_FACTOR * scaleArmyValue(-target.incomingFor(teamId) - target.armyFor(teamId));

			int armyAgainst = target.armiesAgainst(teamId) + target.incomingAgainst(teamId);
			int armyForMe = currentPlanet.armyFor(teamId) + target.incomingFor(teamId);

			if (isOwned && armyAgainst > 0) {
				score += distF * DEFENSE_FACTOR * armyForMe / (armyForMe + armyAgainst);
			}

			int timeUntilEnemyArrives = target.getState().getMovingArmies().stream()
					.filter(army -> !teamId.equals(army.getOwner())) //
					.sorted((army1, army2) -> {
						double dx1 = target.getX() - army1.getX();
						double dy1 = target.getY() - army1.getY();

						double dx2 = target.getX() - army2.getX();
						double dy2 = target.getY() - army2.getY();

						double dist1ToTargetSqr = dx1 * dx1 + dy1 * dy1;
						double dist2ToTargetSqr = dx2 * dx2 + dy2 * dy2;

						return dist1ToTargetSqr < dist2ToTargetSqr ? -1 : 1;
					}).findFirst() //
					.map(army -> {
						double dx = target.getX() - army.getX();
						double dy = target.getY() - army.getY();
						double distance = Math.sqrt(dx * dx + dy * dy);
						return (int) (1000 * distance / description.getMovementSpeed());
					}) //
					.orElse(0);

			int enemyOnTheWay = target.incomingAgainst(teamId, description.getMovementSpeed(),
					timeUntilEnemyArrives + 600);

			boolean weCanArriveInTime = timeUntilEnemyArrives + 2000 > dist / description.getMovementSpeed() * 1000;

			logger.info("WATCH: {} {} {} - {}, {} -> {}", weCanArriveInTime, timeUntilEnemyArrives,
					dist / description.getMovementSpeed() * 1000, lastState.getTimeElapsed(),
					currentPlanet.getState().getPlanetID(), target.getState().getPlanetID());

			if (weCanArriveInTime && enemyOnTheWay > 0
					&& enemyOnTheWay <= currentPlanet.armyFor(teamId) + target.incomingFor(teamId)) {
				score += WATCHDOG_STEP;
			}

			target.setScore(score);
		}

		planetsWithScores.sort((p1, p2) -> p1.getScore() > p2.getScore() ? -1 : 1);
		return planetsWithScores;
	}

	private double scaleArmyValue(int armyValue) {
		return Math.atan(armyValue / ARMY_STRENGTH_SCALE) * 2 / Math.PI;
	}

	@Override
	public void setUp(GameDescription description) {
		this.description = description;
		maxRadius = description.getPlanets().stream().map(p -> p.getRadius()).max((a, b) -> a - b).orElse(0);
		minRadius = description.getPlanets().stream().map(p -> p.getRadius()).min((a, b) -> a - b).orElse(0);
		maxDistance = description.getPlanets().stream().map(p -> {
			double max = 0;
			for (Planet p2 : description.getPlanets()) {
				double d = PlanetAggregate.distance(p, p2);
				max = d > max ? d : max;
			}
			return max;
		}).max((a, b) -> (int) (a - b)).orElse(0.0);
		minDistance = description.getPlanets().stream().map(p -> {
			double min = 0;
			for (Planet p2 : description.getPlanets()) {
				if (p.getPlanetID() == p2.getPlanetID())
					continue;
				double d = PlanetAggregate.distance(p, p2);
				min = d < min ? d : min;
			}
			return min;
		}).min((a, b) -> (int) (a - b)).orElse(0.0);

		double sumDists = 0;
		for (Planet p1 : description.getPlanets()) {
			for (Planet p2 : description.getPlanets()) {
				if (p1.getPlanetID() == p2.getPlanetID()) {
					continue;
				}
				sumDists += PlanetAggregate.distance(p1, p2);
			}
		}
		int planets = description.getPlanets().size();
		double avgDist = sumDists / (planets * (planets - 1));
		logger.info("Average distance: {}", avgDist);
		logger.info("Average time to arrive: {}", avgDist / description.getMovementSpeed());

	}

	@Override
	public String getCurrentStateMetadataJson() {
		final Map<String, Object> params = new HashMap<>();
		params.put("strategy", strategy);
		params.put("enemy", fullEnemy);
		params.put("army", fullArmy);
		try {
			return jsonMapper.writeValueAsString(params);
		} catch (JsonProcessingException e) {
			return "";
		}
	}

	@Override
	public String getCurrentParametersJson() {
		final Map<String, Object> params = new HashMap<>();
		params.put("SMOOTH_FACTOR", SMOOTH_FACTOR);
		params.put("TRYHARD_FACTOR", TRYHARD_FACTOR);
		params.put("RADIUS_FACTOR", RADIUS_FACTOR);
		params.put("PASSIVE_FACTOR", PASSIVE_FACTOR);
		params.put("ARMY_STRENGTH_SCALE", ARMY_STRENGTH_SCALE);
		params.put("DEFENSE_FACTOR", DEFENSE_FACTOR);
		params.put("MAX_ARMY_ON_PLANET_FACTOR", MAX_ARMY_ON_PLANET_FACTOR);
		params.put("AGGRESSIVE_FACTOR", AGGRESSIVE_FACTOR);
		params.put("ENEMY_LOSING_SCALE", ENEMY_LOSING_SCALE);
		params.put("ENEMY_DOMINATING_SCALE", ENEMY_DOMINATING_SCALE);
		try {
			return jsonMapper.writeValueAsString(params);
		} catch (JsonProcessingException e) {
			return "";
		}
	}
}
