package com.twormix.spacebattle;

import com.twormix.spacebattle.messages.GameDescription;
import com.twormix.spacebattle.messages.GameState;

public interface GameLogic {
	
	/**
	 * Update the state info of the game
	 * 
	 * @param state
	 */
	void update(GameState state);

	/**
	 * Setup the game
	 * 
	 * @param description
	 */
	void setUp(GameDescription description);

	/**
	 * Gets metadata for the current game state - used for display / logging
	 * 
	 * @return
	 */
	String getCurrentStateMetadataJson();
	
	/**
	 * Gets the tuning parameters in json, used for display / logging
	 * 
	 * @return
	 */
	String getCurrentParametersJson();
}
