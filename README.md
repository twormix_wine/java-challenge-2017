# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is for the 2017 javachallenge application.

### How do I get set up? ###

Download Eclipse
Install Maven
Grab a glass of Wine
Finished

### How to run the app? ###

For simple run:
`java -jar twormix.jar`

This produces logs: 
`gamescores.log` to save the scores, game specific log file name and game parameters,
`<score>_SCORE_game-<dd>.<hh>-<mm>-<ss>.log` contains the log of the whole game - for replay.

Replay:
`java -jar twormix.jar replay <logfile>`

This replays the match. Stop with `s`, increase replay speed with `d`, play backwards and increase backward speed with `a`. (Press ONCE.)

### Contribution guidelines ###

Drink wine and code

### Who do I talk to? ###

Twormix 1.1 facebook chat